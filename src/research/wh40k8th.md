# Warhammer 40k, 8th edition (incomplete)

#### Edition: **8th**

#### Year: **2017**

#### Summary
It's 40k. Whaddya expect.


## Contents
- .[Mechanics Summary](#mechanics-summary)
  - [The Most Important Rule](#the-most-important-rule)
  - [Turn Procedures](#turn-procedures)
  - [Army Procedures](#army-procedures)
  - [Win Conditions](#win-conditions)
  - [Other](#other)
  - [Unit Structure](#unit-structure)
- [Simulated Game](#simulated-game)
- [Points of Interest](#points-of-interest)
- [Proposed Action](#proposed-action)
- [Lessons to be Learned](#lessons)
  
---
## Mechanics Summary


Here be Primaris.
Keywords are more useful when using software, but are present here to guide the user through the functions of each unit.

### ***The Most Important Rule***
Resolve situations where the rules are unclear by discussing them with your oponent. If discussion does not work, each party rolls a die. The highest number makes the descision.

### Game Procedures[^proc]
This is assuming the players are playing *Only War*, the scenario builder in the Core[^core] Rulebook
   - Select a Mission
     - You and your opponent must agree on a mission, or roll a die to find one.
   - Armies
     - Create an army with the models you have[^???]
   - Battlefield
     - Create the battlefield and set up terrain
     - 4' x 6' tabletop assumed
     - Recommending at least one piece of terrain in each 2' x 2' section
     - Specific battle types may dictate specific setups and rules
   - Primary Objectives
     - both players roll a die. the higher die wins, and chooses/rolls the primary objective.
     - There is a table for this in the Core[^core] rulebook, pg 187.
   - Deployment[^???]
     - The players divide the table in two, and take alternating turns placing their units on the table (where applicable.)
     - Must not be within 12" of the enemy deployment zone.
   - Power Level
     - Determine each armies' power level by adding the *Power Rating* on each of the unit's datasheets.
     - Whichever player has a lower power level is determined the Underdog.
     - On page 187 of the Core[^core] rulebook, there is a block of text which is not a table for some reason, describing the added benefits of being the *underdog*
   - First Turn
     - *Underdog* chooses the first turn
   - Length
     - The Battle lasts for 5 groups of 2 turns, or until an army is rendered inoperable
   - Victory Conditions
     - If one army has slain all its foes, it's a major victory.
     - else the player with most points wins.
     - in the event of a tie, the underdog wins.

Classically, the players choose army lists within a bracket of points - the traditional number being between 500-1500, with a common number being 750-850. This particular bit of tradition is not particularly clear within this tome.

### Turn Procedures[^turn]

As described in the Primer[^primer], Primer Errata[^perrata] and Core Rulebook[^core].

  1. ***Movement***
     - Models[^unit] move in any direction to a distance in inches => their move stat.
     - Models cannot move through other models or terrain features.
     - no part of the models' base can move further than the move stat
     - Models can climb or traverse scenery.
     - if the model's sheet allows for flight or hsa **Fly**, if ignores most terrain limitations
     - if the model has a minimum speed, that model must move at least that distance.
     - models cannot be moved within one inch of an enemy model
     - If you start a movement phase within 1" of an enemy model, you may chooses to move at least 1" away from the model, disallowing **Charge** or **Advance**
     - Proxies are allowed for position if the model cannot stay physically in a location where otherwise it would be able to.
     - models that are reinforcing the main army cannot move or advance in their debut movement phase, as they count as already moved.
  
  2. ***Psychic Phase***
     1. Choose Model/Unit[^unit] and Power
        - *ie, Smite. closest enemy within 18" takes d3 mortal wounds. d6 on critical success.*
     2. Make a psychictest
        - *roll 2d6. on double 1s, counts as critical failure. model suffers d3 mortal wounds. If unit dies, it explodes, causing d3 mortal wounds to all units within 6", which can cause ripple explosions*
     3. If successful, enemy takes an opposed test
        - *roll 2d6. if total is greater than psychic test in part 2, effects of the power is negated.*
     4. resolve power (applies if successful, etc.)
        - *follow description of power*

  3. ***Shooting Phase***
     1. Choose model who wishes to shoot.
        - *cannot choose model that has advanced or fallen back this turn*
     2. choose target
        - *cannot choose targets within 1" of enemy model* 
     3. Choose model's weapon(s)
        - *if a model has more than 1 weapon, it can use all of them*
        - *there are multiple weapon types. please refer to documentation for more information[^weap]*
     4. Resolve Attacks
        1. Make hit roll
     		- *roll a dice[^???], and add modifiers. if roll is >= attacker's bs, a hit is achieved. a roll of 1[^rollof1] is always a failure, regardless of any modifiers*
        2. Make wound roll
           - *roll another dice[^???], add modifiers, and match target toughness on table*
     

             | Attack's Strength                        | d6 roll required |
             |------------------------------------------|:----------------:|
             | if strength is 2x or more of toughness   |        2+        |
             | if strength is greater than toughness    |        3+        |
             | if strength is equal toughness           |        4+        |
             | if strength is less than toughness       |        5+        |
             | if strength is half or less of toughness |        6+        |

        3. Target allocates wounds
           - *should the attack succeed, the player in charge of the targeted unit/model[^unit] may distribute the wounds amongst the models within the unit, should there be more than one.*
           - *if a model/unit targeted has already lost wounds, the wounds must be allocated to that model/unit*

        4. Target(s) make saving throw
           - *player commanding the targeted unit rolls a dice[^???] modified by the attacks' armor penetration value (add or subtract). if the subsequent value is equal to or greater than the the **save** charactaristic of the model, the damage is prevented.*
           - *a roll of 1 always fails.*[^rollof1]

        5. Inflict damage
           - *subtract allocated points of non-saved damage from the units/models targeted. if said unit/model[^unit] has had their wounds reduced to 0 or less, that model is **dead**, and either removed from play or field.*
           - *any damage over the wound count is lost.*
  
  4. ***Charge Phase***
     1. Choose unit to charge with
        - *any unit/model[^unit] between 12" and 1" from an enemy*
        - *cannot choose a unit/model[^unit] that has advanced or fallen back this turn*
     2. Choose Targets
        - *select one or more targets within 12" of charging model*
     3. Enemy resolves Overwatch
        - *upon the beginning of a charge, the targeted unit/model[^unit] may immediately attempt to fire any available ranged weapon at the chosen unit.*
        - *cannot fire if there is a unit within 1"*
        - *resolves like a normal shooting phase*
        - *a **6** is required for success, regardless of firing model's ballistic score or modifiers*
     4. Roll 2d6 and make a charge move
        - *the 2d6 is the maximum charge distance for the selected unit/model[^unit].*
        - *the first model must be within 1" of an enemy, else the charge fails and no unit/model[^unit]s are moved*
        - *once the first unit/model[^unit] is moved, repeat until all units selected to charge have done so*
        - *no unit/model[^unit] can charge more than once in each charge phase*

     *Aside: Heroic Intervention[^heroic]*

  5. ***Fight Phase***
     1. Choose a unit to fight with
         - *any unit within 1" of an enemy can be chosen to fight within this phase.*
         - *All Units, not just controlled by player whose turn this is*
         - *all units that have charged fight first*
         - *after charging units, unit attack order is at players' discretion*
         - *no unit can attack more than once in each fight phase*
         - *once one player runs out of remaining units to fight, the other completes their fight with the remaining models.*
     2. Pile in up to 3"
         - *you may move each unit up to 3"*
     3. Choose Targets
        - *any chosen target must be within 1" of the chosen unit*
        - *or within 1" of a unit that is within 1" of a chosen target*
        - *shouls a unit/model[^unit] be able to make more than one close combat attack, each attack may target different models*
     4. Choose Melee Weapon
         - *on the unit/model[^unit] datasheet, the weapon should be listed has having range & type "melee".*
         - *if a unit has more than one melee weapon, choose the weapon before rolling the dice*
         - *determine split attacks before rolling*
     5. Resolve close combat attacks
		- ***These echo the earlier ranged section. It is not elaborated upon any further in the books***[^???].
        1. Make Hit roll
        2. Make Wound Roll
        3. Enemy Allocates Wound
        4. Enemy Makes Saving throw
        5. Inflict Damage
     7. Consolidate up to 3"
        - *the player whose turn this is may choose to move any model within the fight phase up to 3 inches in any direction so long as the model moves closer to an enemy model*

  6. ***Morale Phase***
      - For each unit[^unit] that has lost models due to death[^death], the unit must take a *Morale* test. 
      - Roll a dice[^???] and add the number of models from the unit slain during the turn
      - If the result of the roll exceeds the highest leadership score in the unit, the test is failed.
      - For each point failed, one model in the unit must flee, and is thus removed from play.
      - The player whose turn this is decides which models flee.
  

### Army Procedures
  - Transports
    - Capacity and special rules are to be listed upon the unit[^unit]'s datasheet.
    - Embarking:
      - if all models in a unit end their move within 3" of a friendly transport, they can embark within it.
      - remove the units from the table and place them to one side, to be restored when disembarking.
      - Embarked units cannot usually do anything, or affected by anthing.
      - If transport is destroyed, any unit within is immediately disembarked. all units must be placed next to the transport, and then the transport is either removed from table, or marked as destroyed. All units forceably disembarked in this manner must roll a d6, and on a 1 are slain.
    - Disembarking
      - any unit that begins the movement phase embarked within a transport vehicle may disembark *before* the transport moves.
      - upon disembarking, all models must be within 3" of the vechicle before movement, and not within 1" of an enemy unit. 
      - any unit unable to comply with the above is slain.
      - units disembarked then act normally, and function through all phases. 
      - Regardless of any additional movement during the movement phase, the unit is counted as 'moved' for rule purposes.
  - Warlord[^warlord]
    - Nominate one of your models to be a Warlord
    - If your warlord is a character, it can use a Warlord Trait[^trait]
    - You can choose or roll a trait from the table, or the unit/model may have a trait within their datasheet.

### Win/Loss Conditions
   See the [Game Procedures](#game-procedures) bullet point on the matter
  

### Other
Much of the main rulebook is dedicated to the setting, and to alternate scenarios for play.

### Unit Structure

  Demo Page[^demo]

  [**Name of unit/model[^unit]**] [**Type of Unit**] [**Power**] 

  [**Profile**]
	

  | name | m  | ws | bs | s | t | w | a | ld | sv |
  |:-----|:--:|:--:|:--:|:-:|:-:|:-:|:-:|:--:|:--:|
  | ???  | 4" | 2+ | 4  | 5 | 6 | 4 | 9 | 9  | 2+ |

  [**Composition**]
  
  "This unit is a unit."

  [**Weapons**]

  | weapon     | range | type  | s  | ap | d | abilities    |
  |:-----------|:-----:|:-----:|:--:|:--:|:-:|--------------|
  | poke stick | Melee | Melee | +2 | -3 | 3 | *Do Things * |

  [**Faction Keywords**]
  > Choas, not primaris, heterodox, dead mans

  [**Keywords**]
  > infantry, charactermans, (name of this creature for some reason)

---
## Simulated Game
Example on Core Rulebook[^core] pg 184.

[How to play Warhammer 40,000 8th edition by *Midwinter Minis*](https://www.youtube.com/watch?v=r0GrQ2DIPyc)

<!-- This should have some expansion -->


	
---
## Points of Interest

  1. ***???*** , [^???]: Don't do this. Please. Use words when you need them, and if you don't want to, use a footnote.
  2. ***Roll of 1***, [^rollof1]: No diceroll can be modified to less than 1, although it can be modified to more than 6. It comes up a lot, but is used as an aside on a page, rather than core concept.
  3. ***[The Most Important Rule](#the-most-important-rule)*** is strangely disconnected with the ruleset and setting at hand. It's not a bad idea per se, however I am not entirely happy with this implementation. <!-- This will require thought. -->
  4. ***Keywords***, Why? What purpose do they serve, that could not be better served with *skills* or *traits*? It makes more sense for a program or web application than for a book. Might as well use *Hashtags* to describe units. #EmperorOfMan #Imperium #MalcadorWasRight #Wardian #SpiritualLiege 
  5. ***Diagrams***, Warhammer 40k 8th Edition is lacking diagrams. Words are used, often in excess, to describe a situation that would be more clearly and succinctly be understood by a diagram.
  6.  ***Layout***, The core rulebook has major rules jammed in the middle behind the setting history, making the rules difficult to find.
  7.  ***Scenarios***, improper emphasis on the *Only War* scenario, rather than the classic *Matched* mode will incur further issues. The description of the scenario only describes power level, and not points. This is inappropriate for a multi-unit wargame. It would work fine for a 'hero' wargame, or a squad-tactics game.
  8.  ***Tone***, The strong tongue-in-cheek tone of 40k is not translated to the rules in any meaningful way. It simultaneously takes itself far to seriously with long stretches of dry, dull text, but punctuates with small bursts of inapporpriately places psuedo-humor.
  9.  ***Accessibility*** There is a lot of strangely disjointed verbage in this edition - things are moved away from each other, and not always described in ways that a complete outsider would understand. There is an Index, but no glossary. There is a lore-based appendix, but not one specifically for gameplay concepts.
  10. ***Lore***, Love it or hate it, Warhammer 40k has some of the deepest, most well-explored lore in wargaming. This edition modifys it some, and I can see trouble brewing on the horizon. Should GW continue on its current path, I cannot see the currently near universally beloved setting staying as such.

---
## Proposed Action


  1. Ignore main mechanics.
  2. Use Diagrams where appropriate
  3. ???
  4. Profit
---

## Lessons
   1. Diagrams are your friends.
   2. Architecture of the main tome is important. Keep gameplay together.
   3. Glossary of game terms/concepts would be very helpful in cultivating a better class of player.
   4. Fluff should not be the primary selling point of your tabletop wargame.
   5. Do not be afraid to deemphasize a singular scenario of battle if it is not favored by the players.
   6. 

---

  

##### 'Warhammer 40,000", "Warhammer 40k", and others are copyright Games Workshop. This is a research document, and thus all items are copyright their respective owners.



[^primer]: Warhammer 40k Battle Primer (Games Workshop, 2018, 8th Edition)
[^perrata]: Warhammer 40,000 Designers' Commentary (Games Workshop, 2018, in reference to [^core] and [^primer])
[^core]: Warhammer 40,000 (Games Workshop, 2018, 8th Edition)
[^turn]: 8th Edition Core Book[^core], Pg 176
[^demo]: 8th Edition Core Book[^core], Pg 174
[^weap]: 8th Edition Core Book[^core], Pg 180
[^???]: Poor wording and overreliance on preestablished formulae within the most important part of text. ie., "Roll a dice", or lack of constructive description, where there was overly descriptive text before.
[^rollof1]: Commonly used axiom
[^heroic]: 8th Edition Core Book[^core], Pg 182, "After the enemy has completed all their charge moves, any of your **characters** that are within 3" may perform a Heroic Intervention. any that do so can move up to 3", so long as they end the move closer to the nearest enemy model.
[^unit]: unit or unit/model refers to the functional group commanded by the player. A unit can contain many models, or it can contain one model. Model are defined by their Datasheet, as displayed in [Unit Structure](#unit-structure)
[^death]: Wounds reduced to zero
[^proc]: 8th Edition Core Book[^core], Pg 187
[^warlord]: 8th Edition Core Book[^core], Pg 186
[^trait]: A Warlord Trait is a preferred tactic or personal ability. 
